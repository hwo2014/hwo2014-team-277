import json
import socket
import sys
import numpy

class WasabiBot(object):

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key
        self.game_tick = 0
        self.last_switch = -1
        self.last_piece = -1
        self.last_lane = -1
        self.last_position = -1
        self.velocity = 0
        self.current_throttle = -1
        self.default_throttle = 0.65
        self.lane_count = 0
        self.next_piece = 0

        self.brake_piece = 39
        self.brake_velocity = 10.0

        # dictionary of pre-calculated throttle values for specific ticks
        self.calculated_throttle_values = {} 

        self.drag = 0.02
        self.power = 0.2

        self.test_state = 0

        # Flag set to true once the JSON log file has been created
        self.logging_initialized = False

    def msg(self, msg_type, data, game_tick = None):
        if game_tick is not None:
            self.send(json.dumps({"msgType": msg_type, "data": data, "gameTick": game_tick}))
        else:
            self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.send(msg + "\n")

    def join(self):

        # return self.msg("joinRace", 
        #     {"botId": { "name": self.name, "key": self.key }, 
        #     "carCount": 1,
        #     "trackName": "germany", 
        #     "password": "schumi4ever"})

        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def throttle(self, throttle, game_tick = None):

        if throttle != self.current_throttle:
            print "changing throttle from {0} to {1}".format(self.current_throttle, throttle)
            self.current_throttle = throttle

        if game_tick is not None:
            self.msg("throttle", throttle, game_tick)
        else:
            self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

        # Close the JSON array and save the log file
        self.log_file.write('];')
        self.log_file.close()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.ping()

    def germany(self, data):

        piece_index = data[0]['piecePosition']['pieceIndex']
        lane_index = data[0]['piecePosition']['lane']['endLaneIndex']
        position = data[0]['piecePosition']['inPieceDistance']

        # update the current speed
        if self.game_tick > 1:

            # calculate the distance traveled last tick
            prev_lane_length = self.pieces[self.last_piece][self.last_lane]
            if piece_index == self.last_piece:
                self.velocity = position - self.last_position
            else:
                self.last_piece = piece_index
                self.velocity = position + (prev_lane_length - self.last_position)
                print "piece {0}, current velocity {1}".format(self.last_piece, self.velocity)

        # velocity is lagging, so update it for the current tick
        self.velocity += (self.current_throttle * self.power - (self.velocity * self.drag))

        # update the last lane data
        self.last_piece = piece_index
        self.last_lane = lane_index
        self.last_position = position

        '''
        Straights for Keimola - each 'straight' is a pre-defined list of track pieces 
        where the car can safely go at max speed, paired with a target exit velocity. 
        '''
        straights = [[[0, 1], 0.50], 
                     [[5], 0.45], 
                     [[9], 0.50], 
                     [[14, 15], 0.50],
                     [[17], 0.50],
                     [[20, 21, 22], 0.50],
                     [[27], 0.50],
                     [[30, 31, 32, 33, 34, 35, 36], 0.50],
                     [[38, 39, 40, 41, 42], 0.50],
                     [[44, 45], 0.55],
                     [[54, 55, 56], 1.0]]

        for x in range(0, len(straights)):

            if piece_index in straights[x][0]:

                target_exit_velocity = straights[x][1] * 10
                indices = straights[x][0]
                index = indices.index(piece_index)
                self.default_throttle = straights[x][1]

                # calculate the number of pixels remaining in the straight
                pixels_remaining = self.pieces[indices[index]][lane_index] - position
                for y in range(index+1, len(indices)):
                    pixels_remaining += self.pieces[indices[y]][lane_index]

                # project the speed at the end of the straight if we brake now
                projected_exit_velocity = self.velocity
                end_pixels = pixels_remaining
                while end_pixels > 0:
                    end_pixels -= projected_exit_velocity
                    projected_exit_velocity = projected_exit_velocity * (1-self.drag)
                    if projected_exit_velocity < 1: break
                projected_exit_velocity /=  ((1-self.drag)*(1-(self.drag/2)))

                # if we'll end under end_velocity, keep the power on, 
                # but only if we haven't already started braking
                if projected_exit_velocity < target_exit_velocity and self.current_throttle != 0.0:
                    self.throttle(1.0)
                    return

                # brake to exit the straight at or below our target end velocity
                if projected_exit_velocity >= target_exit_velocity:
                    if self.current_throttle != 0.0:
                        print "braking to each {0} by the end of piece {1}".format(target_exit_velocity, indices[len(indices)-1])
                    self.throttle(0.0)
                    return

                self.ping()
                return

        if piece_index == 51:
            self.default_throttle = 0.8

        if piece_index == 11:
            self.throttle(0.2)
            return

        # default throttle to the previous straight's target exit velocity
        self.throttle(self.default_throttle)
        return

    def on_car_positions(self, data):

        piece_index = data[0]['piecePosition']['pieceIndex']
        lane_index = data[0]['piecePosition']['lane']['endLaneIndex']
        position = data[0]['piecePosition']['inPieceDistance']

        # update the current speed
        if self.game_tick > 1:

            # calculate the current velocity (distance traveled last tick)
            prev_lane_length = self.pieces[self.last_piece][self.last_lane]
            if piece_index == self.last_piece:
                self.velocity = position - self.last_position
            else:
                self.velocity = position + (prev_lane_length - self.last_position)
                print "tick {2} - piece {0}, current velocity {1}".format(piece_index, self.velocity, self.game_tick)

        # update the last lane data
        self.last_piece = piece_index
        self.last_lane = lane_index
        self.last_position = position

        # switch right (inside for first corner)
        if piece_index == 1 and self.last_switch != 1 and lane_index == 0:
            self.msg('switchLane', 'Right')
            self.last_switch = 1
            return

        # switch left (inside for second corner)
        if piece_index == 11 and self.last_switch != 11:
            self.msg('switchLane', 'Left')
            self.last_switch = 11
            return

        # switch right (inside for third corner)
        if piece_index == 17 and self.last_switch != 17:
            self.msg('switchLane', 'Left')
            self.last_switch = 17
            return

        # default throttle to the previous straight's target exit velocity
        self.throttle(self.default_throttle)
        return


    def on_crash(self, data):
        print("Someone crashed")
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def on_your_car(self, data):
        print "yourCar"
        self.ping()

    def on_game_init(self, data):
        print "gameInit"

        self.lane_count = len(data['race']['track']['lanes'])
        print "{0} lanes".format(self.lane_count)

        # parse the track pieces
        self.pieces = {}
        for piece in data['race']['track']['pieces']:

            # straight piece
            if 'length' in piece:
                lanes = {}
                for x in range(0, self.lane_count):
                    lanes[x] = piece['length']
                print "piece {0}: straight, {1}px".format(len(self.pieces), piece['length'])
                self.pieces[len(self.pieces)] = lanes

            # curved piece
            elif 'radius' in piece:
                lanes = {}
                for x in range(0, self.lane_count):
                    
                    # calculate the arc length
                    radius = piece['radius'] + data['race']['track']['lanes'][x]['distanceFromCenter']
                    arc_len = radius * piece['angle'] * numpy.pi/180
                    if arc_len < 0: arc_len = -arc_len;
                    
                    # Reverse lane order for right turns
                    if piece['angle'] > 0:
                        lanes[self.lane_count-x-1] = arc_len
                    else:
                        lanes[x] = arc_len

                print "piece {0}: curved, {1} deg, {2} rad".format(len(self.pieces), piece['angle'], piece['radius'])
                self.pieces[len(self.pieces)] = lanes

        self.ping()

    def on_spawn(self, data):
        print "spawn"
        self.ping()

    def on_lap_finished(self, data):
        print "lapFinished - {0}".format(data['lapTime']['millis'])
        self.lane_count += 1
        self.ping()

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'yourCar': self.on_your_car,
            'gameInit': self.on_game_init,
            'spawn': self.on_spawn,
            'lapFinished': self.on_lap_finished
        }
        
        # Receive the first message
        socket_file = s.makefile()
        line = socket_file.readline()

        while line:

            # JSON decode the message
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']

            if 'gameTick' in msg:
                self.game_tick = msg['gameTick']

            # Initialize logging if needed
            if self.logging_initialized == False and 'gameId' in msg:
                self.log_file = open('../visualization/logs/{0}.js'.format(msg['gameId']), 'w')
                self.log_file.write('var race_messages = [')
                self.logging_initialized = True

            # Write the JSON data to the log file
            if self.logging_initialized == True:
                self.log_file.write(line + ',')

            # Process the message
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()

            # flush stdout (for cygwin/mintty)
            sys.stdout.flush()

            # Receive the next message
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = WasabiBot(s, name, key)
        bot.run()
