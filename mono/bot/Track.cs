﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using Newtonsoft.Json.Linq;
using System.Linq;

namespace bot
{
    /// <summary>
    /// Track
    /// </summary>
    public class Track
    {
        const double DampingCoefficient = 0.05268;

        #region Properties

        /// <summary>
        /// Track name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Track pieces
        /// </summary>
        public List<Piece> Pieces { get; set; }
        
        /// <summary>
        /// Cars
        /// </summary>
        public Dictionary<string, Car> Cars { get; set; }
        
        /// <summary>
        /// Current tick number
        /// </summary>
        public int Tick { get; set; }

        /// <summary>
        /// Current throttle value
        /// </summary>
        public double Throttle { get; set; }

        /// <summary>
        /// Drag coefficient for this race instance
        /// </summary>
        public double Drag { get; set; }

        /// <summary>
        /// Power coefficient for this instance
        /// </summary>
        public double Power { get; set; }

        /// <summary>
        /// Default throttle for track pieces not covered by stretches
        /// </summary>
        public double DefaultThrottle { get; set; }

        /// <summary>
        /// Throttle multiplier
        /// </summary>
        public double ThrottleMultiplier { get; set; }

        /// <summary>
        /// Track stretches with defined target velocities
        /// </summary>
        public List<Stretch> Stretches { get; set; }

        /// <summary>
        /// Pending right switch
        /// </summary>
        public bool PendingRightSwitch { get; set; }

        /// <summary>
        /// Pending left switch
        /// </summary>
        public bool PendingLeftSwitch { get; set; }

        /// <summary>
        /// Pending turbo
        /// </summary>
        public bool PendingTurbo { get; set; }

        /// <summary>
        /// Number of turbos available
        /// </summary>
        public int TurbosAvailable { get; set; }

        /// <summary>
        /// Flag indicating that a turbo is on
        /// </summary>
        public bool TurboOn { get; set; }

        #endregion

        #region Structs

        /// <summary>
        /// Corner entrance data point
        /// </summary>
        struct CornerEntrance
        {
            /// <summary>
            /// Centripetal acceleration
            /// </summary>
            public double CA;

            /// <summary>
            /// Harmonic oscillator amplitude
            /// </summary>
            public double Amplitude;

            /// <summary>
            /// Harmonic oscillator period
            /// </summary>
            public double Period;

            public CornerEntrance(double ca, double amplitude, double period)
            {
                CA = ca;
                Amplitude = amplitude;
                Period = period;
            }
        }

        #endregion

        #region Fields

        /// <summary>
        /// Index of the next braking tick
        /// </summary>
        int _brakingTick;

        /// <summary>
        /// Corner entrances
        /// </summary>
        List<CornerEntrance> _cornerEntrances;

        #endregion 

        #region Constructor

        public bool Done = false;
        public bool SetVelocityCoefficient = false;
        public bool PassedZero = false;
        public double MaxAngle = 0;

        const int MAX_TICKS = 10000;
        public double[] SlipAngle = new double[MAX_TICKS];
        public double[] SlipDelta = new double[MAX_TICKS];

        bool _setVelocityCoefficient = false;

        int _ticksIntoStretch;

        bool _processedGameInit = false;

        public string BotName { get; set; }

        double _ca;

        Dictionary<double, double> accTicks = new Dictionary<double, double>();
        bool calcedSlipStartEnd = false;

        double slipStart = 0.32;  // ca required to start slipping
        double slipEnd = 0.4;     // ca required to peak at 60 degrees (overly conservative initial estimate)
        double slipAmp = 0;       // amplitude of the current slip
        double slipPeriod = 0;    // period of the current slip

        double maxStretchAngle = -1;

        /// <summary>
        /// Constructor
        /// </summary>
        public Track(string botName)
        {
            Pieces = new List<Piece>();
            Cars = new Dictionary<string, Car>();
            Stretches = new List<Stretch>();
            Throttle = -1;
            PendingLeftSwitch = false;
            PendingRightSwitch = false;
            PendingTurbo = false;
            TurboOn = false;
            Drag = -1;
            Power = -1;
            DefaultThrottle = 0.5;
            BotName = botName;

            _brakingTick = -1;
            _cornerEntrances = new List<CornerEntrance>();
            _ticksIntoStretch = -1;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Process a carPositions message
        /// </summary>
        public void ProcessCarPositions(JArray carPositions)
        {
            // Make sure we've received gameInit
            if (Cars.Count == 0) return;

            // Update the car positions
            foreach (JObject carPosition in carPositions)
            {
                // Get the values from the JObject
                string name = carPosition["id"]["name"].Value<string>();
                if (name != BotName) continue;

                CarPosition currentPosition = new CarPosition();
                double angle = carPosition["angle"].Value<double>();
                double ca = 0;
                currentPosition.Piece.PieceIndex = carPosition["piecePosition"]["pieceIndex"].Value<int>();
                currentPosition.Piece.InPieceDistance = carPosition["piecePosition"]["inPieceDistance"].Value<double>();
                currentPosition.Piece.StartLaneIndex = carPosition["piecePosition"]["lane"]["startLaneIndex"].Value<int>();
                currentPosition.Piece.EndLaneIndex = carPosition["piecePosition"]["lane"]["endLaneIndex"].Value<int>();
                currentPosition.Lap = carPosition["piecePosition"]["lap"].Value<int>();
                currentPosition.Tick = Tick;

                // Calculate velocity
                if (currentPosition.Piece.PieceIndex == Cars[name].Position.Piece.PieceIndex)
                {
                    // All tick movement in the current piece
                    currentPosition.Velocity = currentPosition.Piece.InPieceDistance - Cars[name].Position.Piece.InPieceDistance;
                    if (currentPosition.Velocity > 10 && TurboOn == false) currentPosition.Velocity = 0;
                }
                else
                {
                    // Tick movement split between the current and previous pieces
                    Piece lastPiece = Pieces[Cars[name].Position.Piece.PieceIndex];
                    double lastLaneLength = lastPiece.LaneLenghts[Cars[name].Position.Piece.EndLaneIndex];
                    if (Cars[name].Position.Piece.EndLaneIndex != Cars[name].Position.Piece.StartLaneIndex) lastLaneLength += 2.05;

                    double lastPieceDistance = lastLaneLength - Cars[name].Position.Piece.InPieceDistance;
                    currentPosition.Velocity = currentPosition.Piece.InPieceDistance + lastPieceDistance;
                    if (currentPosition.Velocity > 10 && TurboOn == false) currentPosition.Velocity = 0;
                }

                // Update centripetal acceleration
                Piece thisPiece = Pieces[currentPosition.Piece.PieceIndex];
                if (thisPiece is CurvedPiece)
                {
                    ca = (currentPosition.Velocity * currentPosition.Velocity) / (thisPiece as CurvedPiece).LaneRadii[currentPosition.Piece.EndLaneIndex];
                    if ((thisPiece as CurvedPiece).Angle < 0) ca = -ca;
                }
                

                // Set the updated position
                Cars[name].Position.SlipAngles[currentPosition.Tick] = angle;
                Cars[name].Position.CentAccs[currentPosition.Tick] = ca;
                Cars[name].Position.Piece.PieceIndex = currentPosition.Piece.PieceIndex;
                Cars[name].Position.Piece.InPieceDistance = currentPosition.Piece.InPieceDistance;
                Cars[name].Position.Piece.StartLaneIndex = currentPosition.Piece.StartLaneIndex;
                Cars[name].Position.Piece.EndLaneIndex = currentPosition.Piece.EndLaneIndex;
                Cars[name].Position.Lap = currentPosition.Lap;
                Cars[name].Position.Tick = currentPosition.Tick;
                Cars[name].Position.Velocity = currentPosition.Velocity;
                Cars[name].Position.Velocities[Tick] = currentPosition.Velocity;

                Cars[name].Position.PrintStatus();
            }

            // Update the throttle value and pending switch requests
            updateActions();
        }

        /// <summary>
        /// Process a gameInit message
        /// </summary>
        public void ProcessGameInit(JObject gameInit)
        {
            if (_processedGameInit) return;

            // Track name
            Name = gameInit["race"]["track"]["name"].Value<string>();

            // Lanes
            JArray lanes = gameInit["race"]["track"]["lanes"].Value<JArray>();
            Debug.Assert(lanes.Count > 1);
            double laneWidth = lanes[1]["distanceFromCenter"].Value<double>() - lanes[0]["distanceFromCenter"].Value<double>();
            int laneCount = lanes.Count;

            // Pieces
            foreach (JObject piece in gameInit["race"]["track"]["pieces"].Value<JArray>())
            {
                double length = 0, radius = 0, angle = 0;
                bool _switch = false;
                foreach (JProperty prop in piece.Children<JProperty>())
                {
                    if (prop.Name == "length") length = prop.Value.Value<double>();
                    if (prop.Name == "radius") radius = prop.Value.Value<double>();
                    if (prop.Name == "angle") angle = prop.Value.Value<double>();
                    if (prop.Name == "switch") _switch = true;
                }

                if (length > 0) Pieces.Add(new StraightPiece(length, laneCount, Pieces.Count, _switch));
                else Pieces.Add(new CurvedPiece(angle, radius, laneCount, laneWidth, Pieces.Count, _switch));
            }

            // Cars
            foreach (JObject car in gameInit["race"]["cars"].Value<JArray>())
            {
                string name = car["id"]["name"].Value<string>();
                if (name != BotName) continue;
                string color = car["id"]["color"].Value<string>();
                Cars.Add(name, new Car(name, color));
            }

            // Map out the track
            mapTrack();

            _processedGameInit = true;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Map the track, and segment it into like stretches
        /// </summary>
        private void mapTrack()
        {
            // Split the track into stretches
            List<Piece> pieces = new List<Piece>();
            foreach (Piece piece in Pieces) pieces.Add(piece);

            // Build the stretches
            bool cycle = true;
            while (pieces.Count > 0)
            {
                // Build a straigth stretch
                if (pieces[0] is StraightPiece)
                {
                    Stretch stretch = new Stretch();
                    stretch.Pieces.Add(pieces[0]);

                    // Add straight pieces from the back end of the list
                    if (cycle)
                        for (int x = pieces.Count - 1; x > 0; x--)
                            if (pieces[x] is StraightPiece) { stretch.Pieces.Insert(0, pieces[x]); cycle = false; }
                            else break;

                    // Add straight pieces from the front end of the list
                    for (int x = 1; x < pieces.Count; x++)
                        if (pieces[x] is StraightPiece) stretch.Pieces.Add(pieces[x]);
                        else break;

                    // Remove these pieces from the list
                    foreach (Piece piece in stretch.Pieces)
                        pieces.Remove(piece);

                    Stretches.Add(stretch);
                }

                // Build a curved stretch
                else
                {
                    Stretch stretch = new Stretch();
                    stretch.Pieces.Add(pieces[0]);

                    // Add curved pieces from the back end of the list
                    if (cycle)
                        for (int x = pieces.Count - 1; x > 0; x--)
                            if (pieces[x] is CurvedPiece)
                                if ((pieces[x] as CurvedPiece).Angle == (pieces[0] as CurvedPiece).Angle &&
                                   (pieces[x] as CurvedPiece).Radius == (pieces[0] as CurvedPiece).Radius)
                                { stretch.Pieces.Insert(0, pieces[x]); cycle = false; }
                                else break;
                            else break;

                    // Add curved pieces from the front end of the list
                    for (int x = 1; x < pieces.Count; x++)
                        if (pieces[x] is CurvedPiece)
                            if ((pieces[x] as CurvedPiece).Angle == (pieces[0] as CurvedPiece).Angle &&
                               (pieces[x] as CurvedPiece).Radius == (pieces[0] as CurvedPiece).Radius)
                                stretch.Pieces.Add(pieces[x]);
                            else break;
                        else break;

                    // Remove these pieces from the list
                    foreach (Piece piece in stretch.Pieces)
                        pieces.Remove(piece);

                    Stretches.Add(stretch);
                }
            }

            // Flag the longest straight to use turbo
            Stretches.Where(x => x.Pieces.First() is StraightPiece).OrderByDescending(s => s.Pieces.Sum(p => p.LaneLenghts[0])).First().Turbo = true;
        }

        /// <summary>
        /// Slip angle delta curve period from velocity
        /// </summary>
        /// <param name="velocity">Velocity</param>
        private double velocityToPeriod(double velocity)
        {
            // (coefficients from nonlinear regression)
            return -0.000513709 + 0.0148884 * velocity - 0.000478789 * Math.Pow(velocity, 2);
        }

        /// <summary>
        /// Find the max slip angle over 50 ticks
        /// </summary>
        /// <param name="period">Sine period coefficient</param>
        /// <param name="amplitude">Amplitude coefficient</param>
        /// <returns>Max slip angle</returns>
        private double findMaxAngle(double period, double amplitude)
        {
            // Calculate out to 50 ticks and find the peak
            double peak = 0;
            double slipAngle = 0;
            for (double i = 0; i < 50; i++)
            {
                double delta = amplitude * Math.Exp(-i * 0.05268) * Math.Sin(i * period);
                slipAngle += delta;
                if (slipAngle > peak) peak = slipAngle;
            }
            return peak;
        }

        /// <summary>
        /// Find the amplitude coefficient that yields a less than 60 degree corner
        /// </summary>
        /// <param name="maxCa">Max centripetal acceleration</param>
        /// <param name="period">Sine period coefficient</param>
        /// <returns>Max amplitude coefficient</returns>
        private double findMaxAmp(double maxCa, double period)
        {
            double amp = double.NegativeInfinity;
            double angle = double.NegativeInfinity;

            // 0.1 resolution
            double peak = double.NegativeInfinity;
            for (double x = 0.1; x < 15.0; x += 0.1)
            {
                peak = findMaxAngle(period, x);
                if (peak > angle) { amp = x; angle = peak; }
                if (peak > 62 /* two degrees of wiggle room */) break;
            }

            return amp;
        }

        private bool isAlmostEqual(double a, double b)
        {
            return Math.Abs(a - b) < 1e-4;
        }

        private double getOscillatorValue(double amplitude, double period, double index)
        {
            return amplitude * Math.Pow(Math.E, -index * 0.05268) * Math.Sin(index * period);
        }

        bool inSlip = false;
        int slipOffset = 0;
        int lastProcessedTick = 0;
        double slipVelocity = 0;
        double[] aggregateDeltas = new double[50000];
        List<double[]> projectedDeltas = new List<double[]>();
        double[] projectedAngle = new double[50000];
        double[] slip = new double[50000];

        /// <summary>
        /// Update the throttle value
        /// </summary>
        private void updateActions()
        {
            // Make sure we've processed gameInit
            if (!_processedGameInit) return;

            // Get our car instance
            Car car = Cars[BotName];

            if (Math.Abs(car.Position.SlipAngles[Tick]) > maxStretchAngle) maxStretchAngle = Math.Abs(car.Position.SlipAngles[Tick]);

            #region Calculate Power and Drag

            if (Drag == -1 || Power == -1)
            {
                // Set the throttle to 1.0 to use as a reference to determine power and drag
                Throttle = 1.0;

                // Get power from the first tick with movement
                if (car.Position.Velocity > 0 && Power == -1)
                {
                    Power = car.Position.Velocity / Throttle;
                    Console.WriteLine(string.Format("Power is {0:0.0000}", Power));
                    return;
                }

                // Get drag from the second tick with movement
                if (car.Position.Velocity > 0 && Power != -1 && Drag == -1)
                {
                    Drag = 1 - (car.Position.Velocity - (Throttle * Power)) / (Throttle * Power);
                    Console.WriteLine(string.Format("Drag is {0:0.0000}", Drag));
                }

                // Don't process anything else until we have Power and Drag
                if (Drag == -1 || Power == -1) return;

                // Update throttle settings to accommodate the given power
                ThrottleMultiplier = 10 / (Power / Drag);
            }

            #endregion

            #region Baseline slip angle calculation stuff

            // Calculate slipStart and slipEnd
            if (Tick > 2 && !calcedSlipStartEnd)
            {
                if (car.Position.SlipAngles[Tick - 1] == 0 && accTicks.Count > 0)
                {
                    accTicks = new Dictionary<double, double>();
                }

                if (car.Position.SlipAngles[Tick] != 0)
                {
                    accTicks.Add(car.Position.Velocities[Tick - 1], car.Position.SlipAngles[Tick]);
                }

                if (accTicks.Count == 1)
                {
                    Throttle = 1.0;
                    return;
                }

                if (accTicks.Count == 2)
                {
                    List<double> periods = new List<double>();
                    List<double> amplitudes = new List<double>();
                    for (int x = 0; x < 2; x++) periods.Add(velocityToPeriod(accTicks.Skip(x).First().Key));

                    // Calculate the amplitude of velocity 0
                    double firstDelta0 = accTicks[accTicks.Keys.Skip(0).First()];
                    amplitudes.Add(firstDelta0 / (Math.Pow(Math.E, -0.05268) * Math.Sin(periods[0])));

                    // Calculate the first slip delta of velocity 1
                    double secondDelta0 = amplitudes[0] * Math.Pow(Math.E, -2 * 0.05268) * Math.Sin(periods[0] * 2);
                    double firstDelta1 = accTicks[accTicks.Keys.Skip(1).First()] - secondDelta0;

                    // Calculate the amplitude of velocity 1
                    double amp1 = firstDelta1 / (Math.Pow(Math.E, -0.05268) * Math.Sin(periods[1]));

                    // Calculate the max slip angle of velocities 0 and 2
                    double maxAngle0 = findMaxAngle(periods[0], amplitudes[0]);
                    double maxAngle1 = findMaxAngle(periods[1], amp1);

                    // Get the current lane radius
                    double laneRadius = (Pieces[car.Position.Piece.PieceIndex] as CurvedPiece).LaneRadii[car.Position.Piece.EndLaneIndex];

                    // Calculate the max (60 deg) and min (0 deg) centripetal accleration values 
                    double v0 = accTicks.Keys.Skip(0).First();
                    double v1 = accTicks.Keys.Skip(1).First();
                    double x0 = v0 * v0 / laneRadius;
                    double x1 = v1 * v1 / laneRadius;
                    double y0 = maxAngle0;
                    double y1 = maxAngle1;

                    // Slope and y-intercept of the centripetal-acceleration to max-slip-angle function
                    double slope = (y1 - y0) / (x1 - x0);
                    double yint = y0 - slope * x0;

                    // Calculate slipStart and slipEnd
                    slipEnd = (60 - yint) / slope;
                    slipStart = (0 - yint) / slope;

                    // Set stretch velocity coefficient
                    Stretch.VelocityCoefficient = slipEnd * 0.85;

                    calcedSlipStartEnd = true;
                }
            }

            if (!calcedSlipStartEnd)
            {
                Throttle = 1.0;
                return;
            }

            #endregion

            // Get the current and following two stretches
            Stretch currentStretch = new Stretch();
            Stretch nextStretch = new Stretch();
            Stretch secondStretch = new Stretch();
            for (int x = 0; x < Stretches.Count; x++)
            {
                if (Stretches[x].Pieces.Where(p => p.Index == car.Position.Piece.PieceIndex).Count() == 1)
                {
                    currentStretch = Stretches[x];
                    int nextIndex = x == Stretches.Count - 1 ? 0 : x + 1;
                    nextStretch = Stretches[nextIndex];
                    int secondIndex = nextIndex + 1;
                    if (secondIndex == Stretches.Count) secondIndex = 0;
                    secondStretch = Stretches[secondIndex];
                    break;
                }
            }

            // Check if we've started a new stretch
            if (car.Position.Piece.InPieceDistance < car.Position.Velocity && car.Position.Piece.PieceIndex == currentStretch.Pieces.First().Index)
            {
                int lastPieceIndex = car.Position.Piece.PieceIndex - 1;
                if (lastPieceIndex < 0) lastPieceIndex = Pieces.Count - 1;
                Stretch lastStretch = Stretches.Where(x => x.Pieces.Where(p => p.Index == lastPieceIndex).Count() > 0).First();

                if (maxStretchAngle < 45)
                {
                    lastStretch.StretchMultiplier *= 1.02;
                }
                else if (maxStretchAngle > 55)
                {
                    lastStretch.StretchMultiplier *= 0.98;
                }

                maxStretchAngle = 0;
            }

            // Increment the ticks into stretch counter
            if (car.Position.Velocity > car.Position.Piece.InPieceDistance && car.Position.Piece.PieceIndex == currentStretch.Pieces.First().Index)
            {
                _ticksIntoStretch = 0;
                currentStretch.StretchMultiplier = 1; // reset the stretch multiplier
            }
            else
                _ticksIntoStretch++;

            #region Lane Switching

            // Check if the next stretch is a curve
            if (nextStretch.Pieces[0] is CurvedPiece && _ticksIntoStretch == 0 && nextStretch.Pieces.Count > 2 /* don't switch for short curves */)
            {
                CurvedPiece curvedPiece = nextStretch.Pieces[0] as CurvedPiece;
                if (curvedPiece.LaneRadii[0] > curvedPiece.LaneRadii[curvedPiece.LaneRadii.Count - 1] && car.Position.Piece.EndLaneIndex < curvedPiece.LaneRadii.Count)
                {
                    // We want to switch right
                    PendingRightSwitch = true;
                }
                else if (curvedPiece.LaneRadii[0] < curvedPiece.LaneRadii[curvedPiece.LaneRadii.Count - 1] && car.Position.Piece.EndLaneIndex > 0)
                {
                    // We want to switch left
                    PendingLeftSwitch = true;
                }
            }

            // Check if the next stretch is a single switch piece
            if (nextStretch.Pieces.Count == 1 && nextStretch.Pieces[0].Switch && secondStretch.Pieces[0] is CurvedPiece && secondStretch.Pieces.Count > 2 /* don't switch for short curves */)
            {
                CurvedPiece curvedPiece = secondStretch.Pieces[0] as CurvedPiece;
                if (curvedPiece.LaneRadii[0] > curvedPiece.LaneRadii[curvedPiece.LaneRadii.Count - 1] && car.Position.Piece.EndLaneIndex < curvedPiece.LaneRadii.Count)
                {
                    // We want to switch right
                    PendingRightSwitch = true;
                }
                else if (curvedPiece.LaneRadii[0] < curvedPiece.LaneRadii[curvedPiece.LaneRadii.Count - 1] && car.Position.Piece.EndLaneIndex > 0)
                {
                    // We want to switch left
                    PendingLeftSwitch = true;
                }
            }

            #endregion

            // Get the current target velocity
            double targetVelocity = currentStretch.GetTargetVelocity(car.Position.Piece.EndLaneIndex);

            // Calculate the number of pixels until the end of the stretch
            double pxToCorner = Pieces[car.Position.Piece.PieceIndex].LaneLenghts[car.Position.Piece.EndLaneIndex] - car.Position.Piece.InPieceDistance;
            if (car.Position.Piece.PieceIndex != currentStretch.Pieces.Last().Index)
            {
                int pieceIndex = currentStretch.Pieces.IndexOf(currentStretch.Pieces.Where(p => p.Index == car.Position.Piece.PieceIndex).Single()) + 1;
                if (pieceIndex >= currentStretch.Pieces.Count) pieceIndex = 0;
                for (int x = pieceIndex; x < currentStretch.Pieces.Count; x++)
                    pxToCorner += currentStretch.Pieces[x].LaneLenghts[car.Position.Piece.EndLaneIndex];
            }

            // Calculate the exit velocity if we brake now, with a throttle value of 0.0
            double exitVelocity = car.Position.Velocity;
            while (exitVelocity > nextStretch.GetTargetVelocity(car.Position.Piece.EndLaneIndex))
            {
                exitVelocity *= (1 - Drag);
                pxToCorner -= exitVelocity;
            }

            // Schedule the braking tick
            if (pxToCorner <= nextStretch.GetTargetVelocity(car.Position.Piece.EndLaneIndex) && Throttle != 0 && _brakingTick != Tick || exitVelocity > nextStretch.GetTargetVelocity(car.Position.Piece.EndLaneIndex))
            {
                if (nextStretch.GetTargetVelocity(car.Position.Piece.EndLaneIndex) != currentStretch.GetTargetVelocity(car.Position.Piece.EndLaneIndex))
                {
                    _brakingTick = Tick;
                }
            }

            // Check if it's time to brake
            if (Tick == _brakingTick)
            {
                // Time to brake
                Throttle = 0.0;
            }
            else if (Throttle == 0.0)
            {
                // See if we need to make any throttle adjustments if we're on the last tick of this stretch
                if (car.Position.Velocity + car.Position.Piece.InPieceDistance > Pieces[car.Position.Piece.PieceIndex].LaneLenghts[car.Position.Piece.EndLaneIndex] &&
                   car.Position.Piece.PieceIndex == currentStretch.Pieces[currentStretch.Pieces.Count - 1].Index)
                {
                    double delta = nextStretch.GetTargetVelocity(car.Position.Piece.EndLaneIndex) - car.Position.Velocity;
                    Throttle = (nextStretch.GetTargetVelocity(car.Position.Piece.EndLaneIndex) - car.Position.Velocity + (car.Position.Velocity * Drag)) / Power;
                    if (Throttle > 1) Throttle = 1;
                    if (Throttle < 0) Throttle = 0;
                    return;
                }

                // Braking has started, so continue
                Throttle = 0.0;
            }

            // Fix the throttle mid-stretch, or reset at the start of a stretch
            if ((car.Position.Piece.PieceIndex == currentStretch.Pieces.First().Index || Throttle != 0) && Tick != _brakingTick)
            {
                // See how far we're off from the target velocity
                double velocityDelta = Math.Max(Math.Abs(car.Position.Velocity - targetVelocity), Math.Abs(car.Position.Velocity - (Throttle * 10 * ThrottleMultiplier)));

                // Set the velocity delta to zero if a turbo is on
                if (car.Position.Velocity > targetVelocity && TurboOn == true) velocityDelta = 0;

                // Update throttle if we're off by more than 1e-3
                if (velocityDelta > 1e-1)
                {
                    Throttle = (targetVelocity - car.Position.Velocity + (car.Position.Velocity * Drag)) / Power * ThrottleMultiplier;
                    if (Throttle > 1) Throttle = 1;
                    if (Throttle < 0) Throttle = 0.01; /* greater than 0.0 to avoid trigging auto-braking */
                }

                // Turbo
                if (currentStretch.Turbo == true &&
                    Throttle == 1.0 &&
                    car.Position.Piece.PieceIndex == currentStretch.Pieces.First().Index &&
                    TurboOn == false &&
                    TurbosAvailable > 0)
                {
                    PendingTurbo = true;
                }
            }
        }

        #endregion
    }
}
