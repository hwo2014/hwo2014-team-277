﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace bot
{
    /// <summary>
    /// Message base class
    /// </summary>
    public abstract class Message
    {
        public string ToJson()
        {
            return JsonConvert.SerializeObject(new MessageWrapper(this.Type(), this.Data()));
        }

        protected virtual Object Data()
        {
            return this;
        }

        protected abstract string Type();
    }

    /// <summary>
    /// Message wrapper
    /// </summary>
    public class MessageWrapper
    {
        /// <summary>
        /// Message type
        /// </summary>
        [JsonProperty("msgType")]
        public string MessageType { get; set; }

        /// <summary>
        /// Message data
        /// </summary>
        [JsonProperty("data")]
        public Object Data { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="messageType"></param>
        /// <param name="data"></param>
        public MessageWrapper(string messageType, Object data)
        {
            MessageType = messageType;
            Data = data;
        }
    }

    /// <summary>
    /// BotId
    /// </summary>
    [JsonObject("botId")]
    public class BotId
    {
        /// <summary>
        /// Bot name
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }

        /// <summary>
        /// Bot key
        /// </summary>
        [JsonProperty("key")]
        public string Key { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="name">Bot name</param>
        /// <param name="key">Bot key</param>
        public BotId(string name, string key)
        {
            Name = name;
            Key = key;
        }
    }

    /// <summary>
    /// throttle
    /// </summary>
    public class Throttle : Message
    {
        /// <summary>
        /// Throttle value
        /// </summary>
        [JsonProperty("value")]
        public Double Value { get; set; }

        /// <summary>
        /// msgType
        /// </summary>
        protected override string Type() { return "throttle"; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="value">Throttle value</param>
        public Throttle(double value)
        {
            Value = value;
        }
    }

    /// <summary>
    /// joinRace
    /// </summary>
    [JsonObject("joinRace")]
    public class JoinRace : Message
    {
        /// <summary>
        /// Bot ID
        /// </summary>
        [JsonProperty("botId")]
        public BotId BotId { get; set; }

        /// <summary>
        /// Car count
        /// </summary>
        [JsonProperty("carCount")]
        public int CarCount { get; set; }

        /// <summary>
        /// Track name
        /// </summary>
        [JsonProperty("trackName")]
        public string TrackName { get; set; }

        /// <summary>
        /// Track password
        /// </summary>
        [JsonProperty("password")]
        public string TrackPassword { get; set; }

        /// <summary>
        /// msgType
        /// </summary>
        protected override string Type() { return "joinRace"; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="name">botId.name</param>
        /// <param name="key">botId.key</param>
        /// <param name="trackName">trackName</param>
        /// <param name="carCount">carCount</param>
        public JoinRace(string name, string key, string trackName = "keimola", string trackPassword = "", int carCount = 1)
        {
            BotId = new BotId(name, key);
            CarCount = carCount;
            TrackName = trackName;
            TrackPassword = trackPassword;
        }
    }
}
