﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Linq;

namespace bot
{
    /// <summary>
    /// Car
    /// </summary>
    public class Car
    {
        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Color
        /// </summary>
        public string Color { get; set; }

        /// <summary>
        /// Car position data
        /// </summary>
        public CarPosition Position { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="name">Car name</param>
        /// <param name="color">Car color</param>
        public Car(string name, string color)
        {
            Name = name;
            Color = color;
            Position = new CarPosition();
        }
    }

    /// <summary>
    /// Data on the current piece
    /// </summary>
    public class CarPiece
    {
        /// <summary>
        /// Piece index
        /// </summary>
        public int PieceIndex { get; set; }

        /// <summary>
        /// Distance into the piece
        /// </summary>
        public double InPieceDistance { get; set; }

        /// <summary>
        /// Start lane index
        /// </summary>
        public int StartLaneIndex { get; set; }

        /// <summary>
        /// Start lane index
        /// </summary>
        public int EndLaneIndex { get; set; }
    }

    /// <summary>
    /// Car position data
    /// </summary>
    public class CarPosition
    {
        /// <summary>
        /// Tick -> Slip Angle lookup
        /// </summary>
        public Dictionary<int, double> SlipAngles { get; set; }

        /// <summary>
        /// Tick -> Centripetal Acceleration lookup
        /// </summary>
        public Dictionary<int, double> CentAccs { get; set; }

        /// <summary>
        /// Tick -> velocity lookup
        /// </summary>
        public Dictionary<int, double> Velocities { get; set; }

        /// <summary>
        /// Current velocity
        /// </summary>
        public double Velocity { get; set; }

        /// <summary>
        /// Current lap
        /// </summary>
        public int Lap { get; set; }

        /// <summary>
        /// Current game tick
        /// </summary>
        public int Tick { get; set; }

        /// <summary>
        /// Data on the current piece
        /// </summary>
        public CarPiece Piece { get; set; }

        /// <summary>
        /// Projected slip angle delta values
        /// </summary>
        public List<double> ProjectedSlipDelta { get; set; }

        public CarPosition()
        {
            SlipAngles = new Dictionary<int, double>();
            CentAccs = new Dictionary<int, double>();
            Velocities = new Dictionary<int, double>();
            Piece = new CarPiece();
        }

        /// <summary>
        /// Print the car's current state to the command line
        /// </summary>
        public void PrintStatus()
        {
            Console.WriteLine(string.Format("tick: {4,4}, piece: {0,2:0}, dist: {1,6:0.00}, velocity: {2:0.00}, angle: {3,6:0.00}, lane: {5}", Piece.PieceIndex, Piece.InPieceDistance, Velocity, SlipAngles[Tick], Tick, Piece.EndLaneIndex));
        }
    }
}
