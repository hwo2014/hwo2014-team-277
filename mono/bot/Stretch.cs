﻿using System;
using System.Collections.Generic;
using System.Text;

namespace bot
{
    /// <summary>
    /// Stretch of track with a uniform target velocity
    /// </summary>
    public class Stretch
    {
        /// <summary>
        /// Velocity coefficient for max corner velocity
        /// </summary>
        public static double VelocityCoefficient = 0.475;

        /// <summary>
        /// Variable stretch throttle multiplier
        /// </summary>
        public double StretchMultiplier { get; set; }

        /// <summary>
        /// Pieces in this stretch
        /// </summary>
        public List<Piece> Pieces { get; set; }
        
        /// <summary>
        /// Flag indicating that this is a stretch we ca use turbo on
        /// </summary>
        public bool Turbo { get; set; }

        /// <summary>
        /// Target velocity
        /// </summary>
        public double TargetVelocity { get; set; }

        /// <summary>
        /// Get the target velocity for the current lane
        /// </summary>
        public double GetTargetVelocity(int lane)
        {
            if (Pieces[0] is StraightPiece)
            {
                return 10 * StretchMultiplier;
            }
            else
            {
                double laneRadius = (Pieces[0] as CurvedPiece).LaneRadii[lane];
                double velocitySquared = VelocityCoefficient * laneRadius;
                double maxVelocity = Math.Sqrt(velocitySquared);
                return maxVelocity * StretchMultiplier;
            }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="pieces">Pieces in this stretch</param>
        public Stretch(List<Piece> pieces)
        {
            Pieces = pieces;
            Turbo = false;
            StretchMultiplier = 1;
        }

        /// <summary>
        /// Parameterless constructor
        /// </summary>
        public Stretch()
        {
            Pieces = new List<Piece>();
            Turbo = false;
            StretchMultiplier = 1;
        }
    }
}
