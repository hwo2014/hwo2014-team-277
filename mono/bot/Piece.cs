﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace bot
{
    /// <summary>
    /// Track piece base class
    /// </summary>
    public abstract class Piece
    {
        /// <summary>
        /// Lane lengths
        /// </summary>
        public List<double> LaneLenghts { get; set; }

        /// <summary>
        /// Piece index
        /// </summary>
        public int Index { get; set; }

        /// <summary>
        /// True if this is a switch piece
        /// </summary>
        public bool Switch { get; set; }

        public Piece(int index, bool _switch)
        {
            Index = index;
            Switch = _switch;
            LaneLenghts = new List<double>();
        }
    }

    /// <summary>
    /// Straight track piece
    /// </summary>
    public class StraightPiece : Piece
    {
        /// <summary>
        /// Piece length
        /// </summary>
        public double Length { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="length">Piece length</param>
        public StraightPiece(double length, int laneCount, int index, bool _switch) : 
            base(index, _switch)
        {
            Length = length;
            
            for (int x = 0; x < laneCount; x++)
            {
                LaneLenghts.Add(length);
            }
        }
    }

    /// <summary>
    /// Curved track piece
    /// </summary>
    public class CurvedPiece : Piece
    {
        /// <summary>
        /// Curve angle
        /// </summary>
        public double Angle { get; set; }

        /// <summary>
        /// Curge radius
        /// </summary>
        public double Radius { get; set; }

        /// <summary>
        /// Lane radii
        /// </summary>
        public List<double> LaneRadii { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="angle">Curve angle</param>
        /// <param name="radius">Curve radius</param>
        /// <param name="laneCount">Lane count</param>
        /// <param name="laneWidth">Lane width</param>
        public CurvedPiece(double angle, double radius, int laneCount, double laneWidth, int index, bool _switch) : 
            base(index, _switch)
        {
            Angle = angle;
            Radius = radius;
            LaneRadii = new List<double>();

            double pieceWidth = (laneCount + 1) * laneWidth;

            for (int x = 0; x < laneCount; x++)
            {
                double laneRadius = radius - (pieceWidth / 2) + ((x + 1) * laneWidth);
                double laneLength = laneRadius * (angle * Math.PI / 180);
                if (laneLength < 0) laneLength = -laneLength;
                LaneRadii.Add(laneRadius);
                LaneLenghts.Add(laneLength);
            }

            if (angle > 0)
            {
                LaneLenghts.Reverse();
                LaneRadii.Reverse();
            }
        }
    }
}
