﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net.Sockets;
using System.Text;

namespace bot
{
    class Program
    {
        public static void Main(string[] args)
        {
            // Host
            string host = "testserver.helloworldopen.com"; 
            if(args.Length > 0) host = args[0];

            // Port
            int port = 0;
            if(args.Length > 1) port= int.Parse(args[1]);

            // Bot name
            string botName = "wasabi-autopilot";
            if(args.Length > 2) botName = args[2];

            // Bot key
            string botKey = "t+ANLxuphHc0Hg";
            if (args.Length > 3) botKey = args[3];

            // Track name
            string trackName = "";
            if (args.Length > 4) trackName = args[4];

            // Track password
            string trackPassword = "";
            if (args.Length > 5) trackPassword = args[5];

            // Car count
            int carCount = 1;
            if (args.Length > 6) carCount = int.Parse(args[6]);

            Bot bot = new Bot(botName, botKey, trackName: trackName, trackPassword: trackPassword, port: port, carCount: carCount, host: host);

            Console.WriteLine("Race has finished. Press enter to close.");
            Console.ReadLine();
        }
    }
}
