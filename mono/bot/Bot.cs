using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Text.RegularExpressions;
using bot;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

public class Bot
{
    #region Fields

    // TCP client stuff
    TcpClient _client;
    NetworkStream _stream;
    StreamReader _reader;
    StreamWriter _writer;

    /// <summary>
    /// Current track
    /// </summary>
    Track _track;

    /// <summary>
    /// Current throttle value
    /// </summary>
    double _currentThrottle = -1;

    #endregion

    #region Constructor

    public Bot(string botName, string botKey, string host = "senna.helloworldopen.com", int port = 0, string trackName = "", string trackPassword = "", int carCount = 1)
    {
        Console.WriteLine("Maybe, possibly, hopefully connecting to " + host + ":" + port + " as " + botName + "/" + botKey + ". PLZ PLZ CONNECT!");

        // Connect to the server
        _client = new TcpClient();
        _client.Connect(host, port);
        _client = new TcpClient(host, port);
        _stream = _client.GetStream();
        _reader = new StreamReader(_stream);
        _writer = new StreamWriter(_stream);
        _writer.AutoFlush = true;

        // Instantiate the track
        _track = new Track(botName);

        // Join a race
        join(botName, botKey, trackName, trackPassword, carCount);

        // Log file StreamWriter (Windows only)
        #if __MonoCS__
        #else
        StreamWriter logFile = null;
        string logFilePath = "";
        #endif

        string line;
        while ((line = _reader.ReadLine()) != null)
        {
            // Write this line to log file (Winodws only)
            #if __MonoCS__
            #else
            if (logFile != null) logFile.Write(line + ",\n");
            #endif

            // Update the tick number
            string tick = Regex.Match(line, "\"gameTick\":(\\d+)").Groups[1].Value;
            if (tick != "") _track.Tick = int.Parse(tick);

            if (_track.Done)
            {
                _client.Close();
                return;
            }

            MessageWrapper msg = JsonConvert.DeserializeObject<MessageWrapper>(line);
            switch (msg.MessageType)
            {
                case "carPositions":

                    // Update the car position data
                    JArray carPositions = msg.Data as JArray;
                    _track.ProcessCarPositions(carPositions);

                    // Update the throttle value
                    bool throttled = false;
                    if ((_track.Tick >= 1 && Math.Abs(_track.Throttle -  _currentThrottle) > 1e-3) || 
                        (_track.PendingLeftSwitch == false && _track.PendingRightSwitch == false && _track.PendingTurbo == false))
                    {
                        throttled = true;
                        throttle(_track.Throttle);
                    }

                    if (!throttled)
                    {
                        // Right switch
                        if (_track.PendingRightSwitch)
                        {
                            _track.PendingRightSwitch = false;
                            switchRight();
                        }

                        // Left switch
                        else if (_track.PendingLeftSwitch)
                        {
                            _track.PendingLeftSwitch = false;
                            switchLeft();
                        }

                        // Turbo
                        else if (_track.PendingTurbo)
                        {
                            _track.PendingTurbo = false;
                            _track.TurbosAvailable--;
                            turbo();
                        }

                        else ping();
                    }

                    break;

                case "yourCar":
                    Console.WriteLine("yourCar");

                    string carName = (msg.Data as JObject)["name"].Value<string>();
                    _track.BotName = carName;

                    // Create a log file (Windows only)
                    #if __MonoCS__
                    #else
                    string gameId = Regex.Match(line, "\"gameId\":\"(.+?)\"").Groups[1].Value;
                    logFilePath = @"C:\Users\Marc\Documents\Src\hwo2014-team-277\visualization\logs\" + gameId + ".js";
                    if (!File.Exists(logFilePath))
                    {
                        logFile = File.CreateText(logFilePath);
                        logFile.Write("var race_messages = [\n");
                    }
                    #endif

                    ping();

                    break;

                case "join":
                    Console.WriteLine("join");
                    ping();
                    break;
                case "gameInit":

                    Console.WriteLine("gameInit");

                    // Process gameInit and initialize car/track data
                    JObject gameInit = msg.Data as JObject;
                    _track.ProcessGameInit(gameInit);
                    
                    ping();

                    break;
                case "gameEnd":
                    Console.WriteLine("gameEnd");
                    ping();
                    break;
                case "tournamentEnd":

                    // Close the log file and rename to test.js (Windows only)
                    #if __MonoCS__
                    #else
                    logFile.Write("];\n");
                    logFile.Close();
                    string testRunPath = @"C:\Users\Marc\Documents\Src\hwo2014-team-277\visualization\logs\" + trackName + "-test.js";
                    if (File.Exists(testRunPath)) File.Delete(testRunPath);
                    File.Move(logFilePath, testRunPath);
                    #endif

                    Console.WriteLine("tournamentEnd");
                    ping();
                    break;
                case "gameStart":
                    Console.WriteLine("gameStart");
                    ping();
                    break;
                case "lapFinished":

                    // Display lap time
                    JObject lapFinished = msg.Data as JObject;
                    string name = lapFinished["car"]["name"].Value<string>();
                    string color = lapFinished["car"]["color"].Value<string>();
                    string lapNumber = (int.Parse(lapFinished["lapTime"]["lap"].Value<string>()) + 1).ToString();
                    double ms = (double)lapFinished["lapTime"]["millis"].Value<int>();
                    double sec = ms / 1000;
                    printBorderedMessage(string.Format("{0} car {1} finished lap {2} in {3} seconds", color.ToUpper(), name, lapNumber, sec));
                    ping();
                    break;

                case "turboAvailable":
                    printBorderedMessage("Turbo Available!!");
                    _track.TurbosAvailable++;
                    ping();
                    break;

                case "turboStart":
                    _track.TurboOn = true;
                    break;

                case "turboEnd":
                    _track.TurboOn = false;
                    break;

                default:
                    Console.WriteLine("Received msgType " + msg.MessageType);
                    ping();
                    break;
            }
        }
	}

    #endregion

    #region Private Methods

    /// <summary>
    /// Print a one line bordered message to the console output
    /// </summary>
    private void printBorderedMessage(string message)
    {
        string msgLine = "| " + message + " |";
        string spacerLine = new string('-', msgLine.Length);
        Console.WriteLine(spacerLine);
        Console.WriteLine(msgLine);
        Console.WriteLine(spacerLine);
    }

    /// <summary>
    /// Join a race
    /// </summary>
    /// <param name="botName">Bot name</param>
    /// <param name="botKey">Bot key</param>
    /// <param name="trackName">Track ID, ex. Keimola</param>
    /// <param name="trackPassword">Track password</param>
    /// <param name="cars">Number of cars</param>
    private void join(string botName, string botKey, string trackName = "keimola", string trackPassword = "", int cars = 1)
    {
        JoinRace joinRace = new JoinRace(botName, botKey, trackName, trackPassword, cars);
        string json = joinRace.ToJson();
        if (trackName == "") json = "{\"msgType\": \"join\", \"data\": { \"name\": \"wasabi\", \"key\": \"" + botKey + "\" }}";
        Console.Write("join json: " + json);
        _writer.WriteLine(json);
    }

    /// <summary>
    /// Ping the server
    /// </summary>
    /// <remarks>
    /// During the coding period and qualification rounds, it is required to 
    /// respond to each game tick. This is the "no action" response.
    /// </remarks>
    private void ping()
    {
        _writer.WriteLine("{\"msgType\": \"ping\"}");
    }

    /// <summary>
    /// Fire the turbo
    /// </summary>
    private void turbo()
    {
        printBorderedMessage("Firing Turbo!!");
        _writer.WriteLine("{\"msgType\": \"turbo\", \"data\": \"turbo\"}");
    }

    /// <summary>
    /// Send a request to switch left
    /// </summary>
    private void switchLeft()
    {
        _writer.WriteLine("{\"msgType\": \"switchLane\", \"data\": \"Left\"}");
    }

    /// <summary>
    /// Send a request to switch right
    /// </summary>
    private void switchRight()
    {
        _writer.WriteLine("{\"msgType\": \"switchLane\", \"data\": \"Right\"}");
    }

    /// <summary>
    /// Set a new throttle value
    /// </summary>
    /// <param name="value">throttle value</param>
    private void throttle(double value)
    {
        if (_currentThrottle != value && value != -1)
        {
            printBorderedMessage(string.Format("changing throttle from {0:0.00} to {1:0.00} at tick {2}", _currentThrottle, value, _track.Tick));
            _currentThrottle = value;
        }

        _writer.WriteLine(string.Format("{{\"msgType\": \"throttle\", \"data\": {0:0.0000}, \"gameTick\": {1}}}", value, _track.Tick));
    }

    #endregion

}