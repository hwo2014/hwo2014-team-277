var leading_edge;        // leading edge coordinates
var lane_width;          // lane width
var lane_count;          // lane count
var track_width;         // track width
var canvas;              // track canvas
var context;             // track canvas context
var car_canvas;          // car canvas
var car_context;         // car canvas context
var pieces;              // track pieces
var car_positions;       // car positions from a race
var car_velocities;      // car velocities for a race (from the server vis JSON data)
var current_tick;        // current game tick
var tick_count;          // number of game ticks
var car_size;            // car size
var car_guide_pos;       // car guide flag position
var play_interval;       // setInterval for the playthrough
var car_stats;           // statistics for each car
var angle_offset;        // track angle offset
var track;               // track object from gameInit

// inversion flag for slip angle delta printing debug data
var invert=false;
var printing=false;
var print_index = 0;

// Page init method
function initialize()
{
    // Load the race JSON data
    var regex = new RegExp("[\\?&]gameId=([^&#]*)");
    results = regex.exec(location.search);
    var game_id = decodeURIComponent(results[1].replace(/\+g/, " ", ""));
    var elem = document.createElement("script");
    elem.type = "application/javascript";
    elem.src = "logs/" + game_id + ".js";
    document.body.appendChild(elem);

    // Wait for the JSON to be load
    wait_for_race_json();

    // // Calculate some velocities
    // var throttle = 1.0;
    // var velocity = 0;
    // var acceleration = throttle / 5;
    // var drag = 0.02;

    // // Calculate velocity and distance over time
    // var distance = 0;
    // velocity = 4.607847000139017;
    // for(var x = 0; x < 10; x++)
    // {
    //     velocity += (acceleration - (velocity * drag));
    //     console.log('velocity at ' + x + ': ' + velocity);
    // }
}

// Wait for a race's JSON data to load, then draw the track
function wait_for_race_json()
{
    if(typeof race_messages === 'undefined')
    {
        setTimeout('wait_for_race_json()', 1);
        return;
    }

    // Process the JSON data
    car_positions = {};
    car_velocities = {};
    car_stats = {};
    tick_count = 0;
    for(var x = 0; x < race_messages.length; x++)
    {
        var msg = race_messages[x];

        if(msg.msgType == 'gameInit')
        {
            // Build the track
            track = msg.data.race.track;
            draw_track();

            // Get the car dimensions
            car_size = [msg.data.race.cars[0].dimensions.length, msg.data.race.cars[0].dimensions.width];
            car_guide_pos = msg.data.race.cars[0].dimensions.guideFlagPosition;
            
            // Initialize the car stats objects
            for(var y = 0; y < msg.data.race.cars.length; y++)
            {
                var stats = { 
                    name: msg.data.race.cars[y].id.name,
                    color: msg.data.race.cars[y].id.color,
                    velocity: {},
                    vector_velocity: {},
                    centripital_acceleration: {},
                    centripital_acceleration_delta: {},
                    slip_angle: {},
                    slip_angle_delta: {},
                    projected_slip_delta: {},
                    projected_slip_angle: {},
                    absolute_angle: {},
                    piece: {},
                    distance: {},
                    last_lap_time: 0,
                };
                car_stats[y] = stats;
            }
        }
        else if (msg.msgType == 'carPositions' || msg.msgType == 'fullCarPositions')
        {
            // Add the car position
            car_positions[msg.gameTick] = msg.data;
            if(msg.gameTick > tick_count) tick_count = msg.gameTick;
        }
        else if (msg.msgType == 'carVelocities')
        {
            // Add the velocity data
            car_velocities[msg.gameTick] = msg.data;
        }
        else if (msg.msgType == 'lapFinished')
        {
            // Update the last lap time
            if (msg.data.car.name == 'CruiseControl1')
            {
                car_stats[0].last_lap_time = msg.data.lapTime.millis / 1000;
            }
        }
    }

    // Calculate performance statistics
    calculate_statistics();

    // Initialize the tick counter
    current_tick = 1;
    step_race_forward();
    current_tick = 1;
}

// Calculate preformance statistics
function calculate_statistics()
{
    for(var tick in car_positions)
    {
        if(tick == 'undefined') continue;

        var cur = car_positions[tick];
        var car_count = cur.length;

        // First tick, no velocity data and no angle possible
        if(tick == 1)
        {
            for(var x = 0; x < car_count; x++)
            {
                car_stats[x].velocity[tick] = 0;
                car_stats[x].centripital_acceleration[tick] = 0;
                car_stats[x].centripital_acceleration_delta[tick] = 0;
                car_stats[x].slip_angle[tick] = 0;
                car_stats[x].slip_angle_delta[tick] = 0;
                car_stats[x].projected_slip_delta[tick] = 0;                
                car_stats[x].projected_slip_angle[tick] = 0;                
                car_stats[x].absolute_angle[tick] = angle_offset/(Math.PI/180);
                car_stats[x].piece[tick] = 0;
                car_stats[x].distance[tick] = 0;
            }
        }

        // 2-N tick, calculate velocity and update angle
        else
        {
            var prev = car_positions[tick-1];
            for(var x = 0; x < car_count; x++)
            {
                // Calculate the length of the previous lane
                var prev_piece_index = prev[x].piecePosition.pieceIndex;
                var prev_lane_index = prev[x].piecePosition.lane.endLaneIndex;
                var prev_lane_length = pieces[prev_piece_index][prev_lane_index](1)[3];

                // Distance travelled last tick
                var tick_distance = 0;

                // Full tick took place on the current piece
                if(cur[x].piecePosition.pieceIndex == prev[x].piecePosition.pieceIndex)
                {
                    tick_distance = cur[x].piecePosition.inPieceDistance - prev[x].piecePosition.inPieceDistance;
                }

                // Tick was spread over the current and previous pieces
                else
                {
                    tick_distance = cur[x].piecePosition.inPieceDistance + (prev_lane_length - prev[x].piecePosition.inPieceDistance);
                }

                // Udpate the linear velocity
                car_stats[x].velocity[tick] = tick_distance;

                // Update the slip angle
                if(typeof cur[x].angleOffset != 'undefined')
                    car_stats[x].slip_angle[tick] = cur[x].angleOffset; // server produced JSON playback
                else
                    car_stats[x].slip_angle[tick] = cur[x].angle;
                car_stats[x].projected_slip_angle[tick] = 0;

                // Update the slip angle delta
                if(tick > 1) car_stats[x].slip_angle_delta[tick] = car_stats[x].slip_angle[tick] - car_stats[x].slip_angle[tick-1];
                car_stats[x].projected_slip_delta[tick] = 0;

                // Update the absolute angle
                var coords = pieces[cur[x].piecePosition.pieceIndex][cur[x].piecePosition.lane.endLaneIndex](cur[x].piecePosition.inPieceDistance);
                var piece_angle = coords[2];                
                var absolute_angle = piece_angle + car_stats[x].slip_angle[tick]*Math.PI/180 + angle_offset;   
                car_stats[x].absolute_angle[tick] = absolute_angle/(Math.PI/180);             

                // Update the centripital acceleration magnitude
                var centripital_acceleration = 0;
                var piece = track.pieces[cur[x].piecePosition.pieceIndex];
                if(typeof piece.length === 'undefined')
                {
                    // Get the radius of the current lane
                    var radius = piece.radius;
                    if(piece.angle > 0) radius -= track.lanes[cur[x].piecePosition.lane.endLaneIndex].distanceFromCenter;
                    else radius += track.lanes[cur[x].piecePosition.lane.endLaneIndex].distanceFromCenter;

                    // Calculate centripital acceleration magnitude (v^2/r)
                    centripital_acceleration = tick_distance*tick_distance/radius;
                }
                car_stats[x].centripital_acceleration[tick] = centripital_acceleration;

                // Update the centripital acceleration delta
                if(tick > 1) 
                { 
                    car_stats[x].centripital_acceleration_delta[tick] = 
                        car_stats[x].centripital_acceleration[tick] - car_stats[x].centripital_acceleration[tick-1];

                    // if(track.pieces[cur[x].piecePosition.pieceIndex].angle > 0 && track.pieces[cur[x].piecePosition.pieceIndex-1].angle < 0)
                    // {
                    //     car_stats[x].centripital_acceleration_delta[tick] = 
                    //         -car_stats[x].centripital_acceleration[tick] - car_stats[x].centripital_acceleration[tick-1];
                    // }
                    // else
                    // {

                    // }
                }

                // Update piece index and inPieceDistance
                car_stats[x].piece[tick] = cur[x].piecePosition.pieceIndex;
                car_stats[x].distance[tick] = cur[x].piecePosition.inPieceDistance;
            }
        }
    }

    // Update the projected slip angle delta
    var damping = 0.05268;
    //var damping = 0.052680165;
    //var damping = 0.0526801598487;
    var last_ca = 0;
    var amp = 0;
    var sinc = 0;
    var amp_offset = 0;
    var c1 = 0;
    var c2 = 0;
    var k = 0;
    var apt1;    // a'(t-1)
    var at2;     // a(t-2)
    var apt = 0; // a'(t)
    var sa = 0;  // slip angle

    var pcount = 0;

    for(var tick in car_positions)
    {
        tick = parseInt(tick);

        var ca = car_stats[0].centripital_acceleration_delta[tick];
        if(car_stats[0].centripital_acceleration_delta[tick] > 0)
        {
            var angle = track.pieces[car_positions[tick][0].piecePosition.pieceIndex].angle;
            var inverse = angle < 0 ? true : false;

            if(ca >= 0.320)
            {
                var start = car_stats[0].projected_slip_delta[tick];

                // Get the first two slip angle deltas
                var d1 = car_stats[0].slip_angle_delta[parseInt(tick)+1] - car_stats[0].projected_slip_delta[parseInt(tick)+1];
                var d2 = car_stats[0].slip_angle_delta[parseInt(tick)+2] - car_stats[0].projected_slip_delta[parseInt(tick)+2];

                // Solve for the amplitude coefficient
                amp = d2 / (2*Math.pow(Math.E, -damping*2) * (d1/Math.pow(Math.E, -damping)));
                amp = Math.sqrt(Math.pow(d1/Math.pow(Math.E, -damping), 2) / (1-(amp*amp)));

                // Solve for the sine coefficient
                sinc = Math.asin(d1 / (Math.pow(Math.E, -damping) * amp));
                //sinc = 0.07610144397981654;

                // if(amp_offset == 0) 
                // {
                //     amp_offset = -39*ca + amp;
                //     console.log("amp_offset: " + amp_offset);
                //     console.log(amp);
                // }

                //if(isNaN(amp)) amp = amp_offset + 39*ca;

                // console.log("tick: " + tick + ", d1: " + d1 + ", d2: " + d2 + ", amp: " + amp + ", sinc: " + sinc);

                if(inverse) amp = -amp;
                for(var tick2 in car_positions)
                {


                    tick2 = parseInt(tick2);

                    if(tick2 > tick)
                    {
                        var x = tick2 - tick;

                        var sa_delta = amp*Math.pow(Math.E, -damping*x)*Math.sin(sinc*x);

                        if(inverse) sa_delta = -sa_delta;

                        car_stats[0].projected_slip_delta[tick2] += sa_delta;
                    }
                }
            }

            last_ca = car_stats[0].centripital_acceleration[tick];
        }
        else if (car_stats[0].centripital_acceleration_delta[tick] < -0.1)
        {
            var angle = track.pieces[car_positions[tick-1][0].piecePosition.pieceIndex].angle;
            var inverse = angle < 0 ? true : false;

            console.log(last_ca);

            if(last_ca > 0.320)
            {
                console.log("settling at tick " + tick);
                //if(tick == 269) continue;

                amp = -amp;

                var value = 0;
                for(var tick2 in car_positions)
                {
                    if(parseInt(tick2) > parseInt(tick))
                    {
                        var x = tick2 - tick;

                        var sa_delta = amp*Math.pow(Math.E, -damping*x)*Math.sin(sinc*x);

                        if(inverse) sa_delta = -sa_delta;

                        car_stats[0].projected_slip_delta[tick2] += sa_delta;
                    }
                }
            }
        }

        // Update the projected slip angle
        var angle = 0;
        for(var tick in car_positions)
        {
            angle += car_stats[0].projected_slip_delta[tick];
            car_stats[0].projected_slip_angle[tick] = angle;
        }
    }

    console.log(car_stats[0]);
}

// Draw the plots
function draw_plots()
{
    // Get the tick boundaries
    var start_tick = current_tick - 49;
    if(start_tick <= 1) start_tick = tick_count - start_tick + 1;
    if(start_tick >= tick_count) start_tick = tick_count - start_tick - 1;

    // Get the tick data
    var projected = [];
    var given = [];
    var ticks = [];
    var ymin = Infinity;
    var ymax = -Infinity;
    for(var x = 0; x < 101; x++)
    {
        var tick = start_tick + x;
        if (tick < 1) tick += tick_count;
        else if (tick >= tick_count) tick -= tick_count;

        var tick_data = [tick, {}];
        projected.push(car_stats[0].projected_slip_delta[tick]);
        given.push(car_stats[0].slip_angle_delta[tick]);
        ticks.push('');
    }

    // Draw the chart
    var data = {

        labels: ticks,
        datasets: [

            {
                fillColor : "rgba(0,0,0,0.0)",
                strokeColor : "blue",
                pointColor : "blue",
                pointStrokeColor : "#fff",
                data: projected,
            },
            {
                fillColor : "rgba(0,0,0,0.0)",
                strokeColor : "red",
                pointColor : "red",
                pointStrokeColor : "#fff",
                data : given,
            }
        ]
    };

    // Get a context to the top plot canvas and then clear it
    context1 = document.getElementById('plot1').getContext('2d');
    var options = 
    { 
        animation: false,
    };
    //new Chart(context1).Line(data, options);

    //console.log(data);
}

// Mouse wheel handler to scroll through game ticks
function tick_scroll(event)
{
    if('wheelDelta' in event)
    {
        var scrolled = -event.wheelDelta / 120;
        current_tick += scrolled;
        if(current_tick == 0) current_tick = tick_count;
        if(current_tick > tick_count) current_tick = 1;
        refresh_race();
    }
}

// Replay race from the beginning
function replay_race()
{
    console.log("replay race");
    current_tick = 1;
    play_interval = setInterval(step_race_forward, 15);
}

// Pause race playback
function pause_race()
{
    console.log("pause race");
    clearInterval(play_interval);
}

// Resume race playback
function resume_race()
{
    console.log("resume race");
    play_interval = setInterval(step_race_forward, 15);
}

// Stop race playback
function stop_race()
{
    console.log("stop race");
    clearInterval(play_interval);
    current_tick = 1;
    step_race_forward();
}

// Refresh the race display
function refresh_race()
{
    // Reset the car later
    car_context.clearRect(0, 0, car_canvas.width, car_canvas.height);    

    // Get the current car position data
    var cars = car_positions[current_tick];

    // Draw the cars
    for(var x = 0; x < cars.length; x++)
    {
        var car = cars[0];
        var slip_angle = car_stats[x].slip_angle[current_tick];

        // Draw the car
        draw_car(car.id.color, car.piecePosition.pieceIndex, 
                 car.piecePosition.lane.startLaneIndex, car.piecePosition.inPieceDistance, 
                 slip_angle*Math.PI/180);
    }

    // Update the tick count
    document.getElementById('tick-count').innerHTML = current_tick + ' of ' + tick_count;

    // // Output slip angle deltas
    // if(printing)
    // {
    //     //var delta = car_stats[0].projected_slip_delta[current_tick].toFixed(9);
    //     //var delta = car_stats[0].slip_angle_delta[current_tick].toFixed(9);
    //     //var delta = car_stats[0].slip_angle_delta[current_tick].toFixed(9) - car_stats[0].projected_slip_delta[current_tick].toFixed(9);
    //     var delta = car_stats[0].slip_angle_delta[current_tick].toFixed(9);
    //     //delta = delta.toFixed(9);
    //     if (delta < 0 && printIndex == 0) invert = true;

    //     if (invert) delta = -delta;

    //     //delta = parseFloat(delta) - parseFloat(car_stats[0].projected_slip_delta[current_tick]);
    //     //delta = parseFloat(delta) - parseFloat(car_stats[0].projected_slip_delta[current_tick - printIndex]);

    //     $('#debugging').append('{' + printIndex + ',' + delta + '}, ');
    //     printIndex += 1;
    //     //$('#debugging').append(delta + ', ');
    // }
    // //if(cars[0].piecePosition.pieceIndex >= 4 && cars[0].piecePosition.pieceIndex <= 7)
    // if(current_tick >= 85 && current_tick <= 561)
    // {
    //     if(printing == false) 
    //     {
    //         $('#debugging').append('{' + 0 + ',' + 0 + '}, ');
    //         printIndex = 1;
    //     }
    //     printing = true;
    // }
    // else printing = false;
    // if(cars[0].piecePosition.pieceIndex == 2) $('#debugging').html('');

    // Update the stats
    $('#slip-angle').html(car_stats[0].slip_angle[current_tick].toFixed(6) + '&deg;');
    $('#slip-angle-projected').html(car_stats[0].projected_slip_angle[current_tick].toFixed(6) + '&deg;');
    $('#slip-angle-delta').html(car_stats[0].slip_angle_delta[current_tick].toFixed(9) + '&deg;');
    $('#slip-angle-delta-projected').html(car_stats[0].projected_slip_delta[current_tick].toFixed(9) + '&deg;');
    //$('#absolute-angle').html(car_stats[0].absolute_angle[current_tick].toFixed(3) + '&deg;');
    $('#velocity').html(car_stats[0].velocity[current_tick].toFixed(6));
    $('#centripetal-acceleration').html(car_stats[0].centripital_acceleration[current_tick].toFixed(6));
    $('#centripetal-acceleration-delta').html(car_stats[0].centripital_acceleration_delta[current_tick].toFixed(6));
    $('#current-piece').html(car_stats[0].piece[current_tick]);
    $('#in-piece-distance').html(car_stats[0].distance[current_tick].toFixed(3));
    $('#last-lap-time').html(car_stats[0].last_lap_time.toFixed(3));

    // Draw the plots
    draw_plots();
}

// Step the race forward by one tick
function step_race_forward()
{ 
    // Refresh the race
    refresh_race();

    // Stop the animation if we're at the end
    current_tick++;
    if(current_tick > tick_count) clearInterval(play_interval);
}

// Draw a car on the track
function draw_car(color, piece, lane, position, angle)
{
    // Get the car size
    var length = car_size[0];
    var width = car_size[1];

    // Get the coordinates of the lane position
    var coords = pieces[piece][lane](position);
    var piece_angle = coords[2];

    // Get the absolute angle
    var absolute_angle = angle + piece_angle;

    // Calculate the car corners
    var front_left = [coords[0] + car_guide_pos, coords[1] - width/2];
    var front_right = [coords[0] + car_guide_pos, coords[1] + width/2];
    var back_left = [coords[0] - (length-car_guide_pos), coords[1] - width/2];
    var back_right = [coords[0] - (length-car_guide_pos), coords[1] + width/2];

    // Rotate the car corners
    if(absolute_angle != 0)
    {
        front_left = rotate(front_left, coords, absolute_angle);
        front_right = rotate(front_right, coords, absolute_angle);
        back_left = rotate(back_left, coords, absolute_angle);
        back_right = rotate(back_right, coords, absolute_angle);
    }

    // Draw the car
    car_context.save();
    car_context.beginPath();
    car_context.moveTo(back_left[0], back_left[1]);
    car_context.lineTo(front_left[0], front_left[1]);
    car_context.lineTo(front_right[0], front_right[1]);
    car_context.lineTo(back_right[0], back_right[1]);
    car_context.closePath();
    car_context.fillStyle = color;
    car_context.fill();
    car_context.restore();
}

// Draw track pieces onto the canvas
function draw_track()
{
    // Get a context to the track canvas and then clear it
    canvas = document.getElementById('track');
    context = canvas.getContext('2d');
    context.clearRect(0, 0, canvas.width, canvas.height);    

    // Get a context to the car canvas and then clear it
    car_canvas = document.getElementById('cars');
    car_context = car_canvas.getContext('2d');
    car_context.clearRect(0, 0, car_canvas.width, car_canvas.height);    

    // Calculate the lane width
    lane_width = 20; // default to 20
    lane_count = track.lanes.length;
    if(lane_count > 1) lane_width = track.lanes[1].distanceFromCenter - track.lanes[0].distanceFromCenter;

    // Calculate the track width / length
    track_width = (lane_count + 1) * lane_width;
    var length = track.pieces[0].length;

    // Track specific layout
    var start_x = 0; var start_y = 0;
    if(track.id == 'keimola')
    {
        start_x = 630; start_y = 75;
        $('#track').attr('height', '770');
        $('#cars').attr('height', '770');
        $('#track-container').css('height', '770');
    }
    else if(track.id == 'germany')
    {
        start_x = 750; start_y = 485;
        $('#track').attr('height', '900');
        $('#cars').attr('height', '900');
        $('#track-container').css('height', '900');
    }
    else if(track.id == 'usa')
    {
        start_x = 680; start_y = 70;
        $('#track').attr('height', '900');
        $('#cars').attr('height', '900');
        $('#track-container').css('height', '900');
    }
    else if(track.id == 'france')
    {
        start_x = 450; start_y = 790;
        $('#track').attr('height', '930');
        $('#track').attr('width', '1250');
        $('#cars').attr('height', '930');
        $('#cars').attr('width', '1250');
        $('#track-container').css('height', '930');
    }

    // Figure out the leading edge coordinates, assuming a horizontal starting piece
    leading_edge = [[start_x, start_y+20], [start_x + track_width, start_y+20]];
    angle_offset = track.startingPoint.angle*Math.PI/180;
    leading_edge_angle = track.startingPoint.angle*Math.PI/180;
    leading_edge[1] = rotate(leading_edge[1], leading_edge[0], leading_edge_angle);

    // Draw the pieces
    pieces = [];
    for(var x = 0; x < track.pieces.length; x++)
    {
        var first = x == 0;

        switched = false;
        if(typeof track.pieces[x].switch != 'undefined') switched = true;

        if(typeof track.pieces[x].length === 'undefined')
        {
            // Curved piece
            draw_curved_piece(track.pieces[x].angle, track.pieces[x].radius, switched);
        }
        else
        {
            // Straight piece
            draw_straight_piece(track.pieces[x].length, first, switched)
        }
    }
}

// Rotate a point 'point' around an origin 'origin' by angle 'angle'
function rotate(point, origin, angle)
{
    var cx = Math.cos(angle) * (point[0] - origin[0])
           - Math.sin(angle) * (point[1] - origin[1]) 
           + origin[0];

    var cy = Math.sin(angle) * (point[0] - origin[0]) 
           + Math.cos(angle) * (point[1] - origin[1]) 
           + origin[1];

    var p = [cx, cy];
    return p;
}

// Draw a curved piece
function draw_curved_piece(curve_angle, radius, switched)
{
    // Check if this is a right or left curve
    right = curve_angle > 0 ? true : false;

    // Trailing outer corner
    var trailing_outer = leading_edge[0];
    var trailing_inner = leading_edge[1];

    // Rotation angle
    var angle = Math.PI - Math.atan2(trailing_outer[0] - trailing_inner[0], trailing_outer[1] - trailing_inner[1]);

    // Circle properties
    var inner_radius = radius - track_width / 2;
    var outer_radius = radius + track_width / 2;
    var center_x = trailing_outer[0];
    var center_y = right ? trailing_outer[1] + inner_radius + track_width : trailing_outer[1] - inner_radius;

    // Rotate the center if needed
    if(angle != 0)
    {
        rotated = rotate([center_x, center_y], trailing_outer, angle);
        center_x = rotated[0];
        center_y = rotated[1];
    }

    var start_angle = right ? -Math.PI/2 + angle : Math.PI/2 + angle;
    var end_angle = right ? curve_angle*Math.PI/180-Math.PI/2 + angle : curve_angle*Math.PI/180+Math.PI/2 + angle;

    // Draw the trailing edge
    context.beginPath();
    context.moveTo(trailing_outer[0], trailing_outer[1]);
    context.lineTo(trailing_inner[0], trailing_inner[1]);

    // Draw the inner arc
    context.arc(center_x, center_y, inner_radius, start_angle, end_angle, right ? false : true);

    // Draw the outer arc
    context.arc(center_x, center_y, outer_radius, end_angle, start_angle, right ? true : false);

    // Fill the segment
    context.closePath();
    var fill_style = 'rgb(17, 17, 17)';
    if(switched) fill_style = 'rgb(35, 35, 35)';
    context.fillStyle=fill_style;    
    context.fill();

    // Draw the lanes
    coord_functions = [];
    for(var x = 0; x < lane_count; x++)
    {
        // Draw the lane
        context.beginPath();
        context.arc(center_x, center_y, inner_radius + lane_width * (x + 1), start_angle, end_angle, right ? false : true);
        context.strikeWidth = 2;
        context.strokeStyle = 'rgb(102, 102, 102)';
        context.stroke();

        // Build the coordinate retrieval function
        var get_coords = function(rad)
        {
            return function(position) 
            {
                // Calculate arc length
                var rev = false;
                var arc_len = rad * (end_angle - start_angle); 
                if(arc_len < 0) { arc_len = -arc_len; rev = true; }

                // Calculate the position angle
                var pos_angle = (position / arc_len) * (end_angle - start_angle) + start_angle;

                // Calculate the coordinates
                var x = center_x + Math.cos(pos_angle) * rad;
                var y = center_y + Math.sin(pos_angle) * rad;

                // Update the angle
                if(!rev) pos_angle += Math.PI/2;
                else pos_angle -= Math.PI/2;

                // Return the coordinates
                return [x, y, pos_angle, arc_len];
            };
        }(inner_radius + lane_width * (x + 1));

        if(right) 
        {
            coord_functions.reverse();
            coord_functions.push(get_coords);
            coord_functions.reverse();
        }
        else
        {
            coord_functions.push(get_coords);
        }
        
    }
    pieces.push(coord_functions);

    // Draw the leading edge
    var inner_x = center_x + Math.cos(end_angle) * inner_radius;
    var inner_y = center_y + Math.sin(end_angle) * inner_radius;
    var outer_x = center_x + Math.cos(end_angle) * outer_radius;
    var outer_y = center_y + Math.sin(end_angle) * outer_radius;
    context.beginPath();
    context.moveTo(inner_x, inner_y);
    context.lineTo(outer_x, outer_y);
    context.closePath();
    context.lineWidth = 1;
    context.strokeStyle = 'rgb(51, 51, 51)';
    context.stroke();

    // Inner corners
    var leading_outer = [outer_x, outer_y];
    var leading_inner = [inner_x, inner_y];

    // Update the leading edge
    if(right)
        leading_edge = [[leading_outer[0], leading_outer[1]], [leading_inner[0], leading_inner[1]]];    
    else
        leading_edge = [[leading_inner[0], leading_inner[1]], [leading_outer[0], leading_outer[1]]];    
}

// Draw a straight piece
function draw_straight_piece(length, first, switched)
{
    // Trailing outer corner
    var trailing_outer = leading_edge[0];
    var trailing_inner = leading_edge[1];

    // Rotation angle
    var angle = Math.PI - Math.atan2(trailing_outer[0] - trailing_inner[0], trailing_outer[1] - trailing_inner[1]);

    // Leading outer corner
    var leading_outer = [trailing_outer[0] + length, trailing_outer[1]];
    if(angle != 0) leading_outer = rotate(leading_outer, trailing_outer, angle);

    // Leading inner corner
    var leading_inner = [trailing_inner[0] + length, trailing_inner[1]];
    if(angle != 0) leading_inner = rotate(leading_inner, trailing_inner, angle);

    // Draw the rectangle
    context.beginPath();
    context.moveTo(trailing_outer[0], trailing_outer[1]);
    context.lineTo(leading_outer[0], leading_outer[1]);
    context.lineTo(leading_inner[0], leading_inner[1]);
    context.lineTo(trailing_inner[0], trailing_inner[1]);
    context.closePath();
    var fill_style = 'rgb(17, 17, 17)';
    if(switched) fill_style = 'rgb(35, 35, 35)';
    context.fillStyle=fill_style;
    context.fill();

    // Draw the leading edge border
    context.beginPath();
    context.moveTo(leading_inner[0], leading_inner[1]);
    context.lineTo(leading_outer[0], leading_outer[1])
    context.lineWidth = 1;
    context.strokeStyle = 'rgb(51, 51, 51)';
    context.stroke();

    // If this is the first piece, draw the starting line
    if(first)
    {
        context.beginPath();
        context.moveTo(trailing_inner[0], trailing_inner[1]);
        context.lineTo(trailing_outer[0], trailing_outer[1])
        context.lineWidth = 5;
        context.strokeStyle = 'rgba(255, 255, 0, 100)';
        context.stroke();    
    }

    // Draw the lanes
    coord_functions = [];
    for(var x = 0; x < lane_count; x++)
    {
        // Calculate lane start and end points
        var start = [trailing_outer[0], trailing_outer[1] + lane_width * (x + 1)];
        var end = [trailing_outer[0] + length, trailing_outer[1] + lane_width * (x + 1)];
        if(angle != 0)
        {
            start = rotate(start, trailing_outer, angle);
            end = rotate(end, trailing_outer, angle);
        }

        // Draw the lane
        context.beginPath();
        context.moveTo(start[0], start[1]);
        context.lineTo(end[0], end[1])
        context.lineWidth = 2;
        context.strokeStyle = 'rgb(102, 102, 102)';
        context.stroke();

        // Build the coordinate retrieval function
        var get_coords = function(trailing_outer, x, angle)
        {
            return function(position) 
            {
                // Calculate the position
                var start = [trailing_outer[0], trailing_outer[1] + lane_width * (x + 1)];
                var end = [trailing_outer[0] + position, trailing_outer[1] + lane_width * (x + 1)];
                if(angle != 0)
                {
                    start = rotate(start, trailing_outer, angle);
                    end = rotate(end, trailing_outer, angle);
                }
                return [end[0], end[1], angle, length];
            };
        }(trailing_outer, x, angle);
        coord_functions.push(get_coords);         
    }
    pieces.push(coord_functions);

    // Update the leading edge
    leading_edge = [[leading_outer[0], leading_outer[1]], [leading_inner[0], leading_inner[1]]];
}